# Determines what prize customer have won.
#
# @param prediction_score [String] what score was predicted by customer, ex. '1:0'
# @param real_score [String] actual score, ex. '11:20'
# @return [Fixnum] 0 - no prize, 1 - small prize, 3 - big prize
def winner_or_not(prediction_score, real_score)
  return 3 if prediction_score == real_score
  prediction_score = prediction_score.split(':').map(&:to_i)
  real_score = real_score.split(':').map(&:to_i)
  if prediction_score[0] < prediction_score[1] && real_score[0] < real_score[1] ||
    prediction_score[0] == prediction_score[1] && real_score[0] == real_score[1] ||
    prediction_score[0] > prediction_score[1] && real_score[0] > real_score[1]
    1
  else
    0
  end
end
