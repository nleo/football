require_relative '../football'

describe 'winner_or_not' do
  it('1:1, 2:2 should return 1'){ winner_or_not('1:1', '2:2').should == 1 }
  it('8:4, 8:4 should return 3'){ winner_or_not('8:4', '8:4').should == 3 }
  it('1:1, 3:1 should return 0'){ winner_or_not('1:1', '3:1').should == 0 }
  it('8:1, 3:1 should return 1'){ winner_or_not('8:1', '3:1').should == 1 }
end
